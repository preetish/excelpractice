package core;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

import java.io.IOException;

import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class CommentedXmlReader {
	static String AttributeTagName;
	static String AttributeId;
	static ByteArrayOutputStream baos;
      static String comment;
	static ArrayList<String> list;

	
	public CommentedXmlReader(String AttributeTagName,String AttributeId) {
		CommentedXmlReader.AttributeTagName=AttributeTagName;
		CommentedXmlReader.AttributeId=AttributeId;

	}
	
	
public static  ArrayList<String> output(String path,String AttributeTagName,String AttributeId) throws XMLStreamException, FactoryConfigurationError, ParserConfigurationException, SAXException, IOException {
		
	
	  list = new ArrayList<String>();
		
	
		XMLStreamReader xr = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(path),"UTF-8");
		
      
		while (xr.hasNext()) {
	
      	    xr.close();
			
			if (xr.next() == XMLStreamConstants.COMMENT) {
      		
      	
            comment  = xr.getText().toString();
          
               	System.out.println(comment);
      	
      	
      	
      
       	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
       	DocumentBuilder 	builder; 
       	
       	builder = factory.newDocumentBuilder();  
   
       	
   //	comment=comment.trim().replaceFirst(".^([\\W]+)<","<");
       	comment=comment.trim().replaceAll("KEYWORD SEPERATOR: bullet", "");
     	
         xr.close();
    	
     	
     	Document doc1 = builder.parse( new InputSource( new StringReader(comment)));
       	
     	doc1.getDocumentElement().normalize();
     	
       	NodeList nodeList1  = doc1.getElementsByTagName(AttributeTagName);
 	      
 	       for (int i=0; i<nodeList1.getLength(); i++) 
  {
      //Get element
 	    	  
      Node nNode = nodeList1.item(i);
      Element  eElement1 = (Element) nNode;
      
     
      
	     baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	   
	    PrintStream old = System.out;
	    
	    System.setOut(ps);
	    
	    System.out.println("aq id : " + eElement1.getAttribute(AttributeId));
	  
	    System.out.flush();
	    
	    System.setOut(old);
	    
	    
	    
           
      if(eElement1.hasAttributes()) {
    	  String pkm =baos.toString();
     
    	      System.out.println("Pass its present");
    	     list.add( pkm.toString());
    	  System.out.println("String value: " + pkm.toString());
    
      }else {
    	  
   	   System.out.println("Fail not present");
                     }
                }
      	    }
        }       
       
        return list;
       
    }
}