package core;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RandomWebElement {
	static Random sRandom = new Random();
	static int rowCount = 0, columnCount = 0;
	final static String selectedCell = "";

	public static WebElement select(WebDriver driver, String RowTable, String colTable) {
	
		

		boolean isCellEmpty = true;

		List<WebElement> element = driver.findElements(By.xpath(RowTable));
		int randonIndex = 0;

		do {
			randonIndex = sRandom.nextInt(element.size());
			isCellEmpty = element.get(randonIndex).getText().isEmpty();
		} while (isCellEmpty);

		System.out.println(element.get(randonIndex).getText());
		
		getRowColumnCount(driver, RowTable, colTable);

		return element.get(randonIndex);
	}

	
	
	private static void getRowColumnCount(WebDriver driver, String RowTable, String colTable) {
		WebElement TogetRows = driver.findElement(By.xpath(RowTable));
		List<WebElement> TotalRowsList = TogetRows.findElements(By.tagName("tr"));
		rowCount = TotalRowsList.size();

		WebElement ToGetColumns = driver.findElement(By.xpath(colTable));
		List<WebElement> TotalColsList = ToGetColumns.findElements(By.tagName("td"));
		columnCount = TotalColsList.size();
	}
	
}
