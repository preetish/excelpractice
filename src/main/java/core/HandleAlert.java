package core;


import org.openqa.selenium.Alert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HandleAlert {

	
WebDriver driver;
	


public HandleAlert(final WebDriver ldriver) {
	}
	
	public static boolean isAlertPresentDismiss(final WebDriver driver){
	    boolean foundAlert = false;
	    WebDriverWait wait = new WebDriverWait(driver, 10);
	    try {
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert obj = driver.switchTo().alert();
			
	        obj.dismiss();
	        foundAlert = true;
	    } catch (TimeoutException e) {
	        foundAlert = false;
	        
	    }
	    return foundAlert;
	}
	
	
	public static boolean isAlertPresentAccept(final WebDriver driver) throws InterruptedException{
	    boolean foundAlert = false;
	    WebDriverWait wait = new WebDriverWait(driver,95);
	    try {
	    	
	    	 
	    	 wait.until(ExpectedConditions.alertIsPresent());
	    	
	    	 Alert obj = driver.switchTo().alert();
	    	
	    	 String alertText = obj.getText();
	    	 System.out.println("Alert text is " + alertText);
	    	
	    	    Thread.sleep(4000); 
	    	
	         obj.accept();
	        
	          foundAlert = true;
	    } catch (TimeoutException e) {
	          foundAlert = false;
	        
	    }
	    return foundAlert;
	}
	
}
	