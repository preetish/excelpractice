package core;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class FileExtention {

	
	public static String File_Extention(String Dir,final String ExtentionType) throws IOException {
		
		File f = new File(Dir);
		FilenameFilter textFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.endsWith(ExtentionType)) {
					return true;
				} else {
					return false;
				}
			}
		};

		File[] files = f.listFiles(textFilter);
		for (File file : files) {
			if (file.isDirectory()) {
				System.out.print("directory:");
			} else {
				System.out.print("file:");
			}
			System.out.println(file.getCanonicalPath());
		}
		return ExtentionType;

	
	}
	
}
