package core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;



public class WritePropertiesFile {
	
	String url=null;
	

	public static String WriteUrlFireFoxInPropertiesFile(String url) {
		try {
		Properties properties = new Properties();
		properties.setProperty("FireFoxTableContainUrl",url);
		
		File file = new File("D:\\Edit_GenieWorkSpace\\EditGenieAutomationProject\\Configfolder\\UrlLinkGenration\\FireFoxUrlGenrationForTableContain_Footnote.properties");
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;

	}
	
	
	
	public static String WriteUrlChromeInPropertiesFile(String url) {
		try {
		Properties properties = new Properties();
		properties.setProperty("ChromeTableContainUrl",url);
		
		File file = new File("D:\\Edit_GenieWorkSpace\\EditGenieAutomationProject\\Configfolder\\UrlLinkGenration\\ChromeUrlGentrationForTableContain_Footnote.properties");
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;

	}
	
	public static String WritePEUrlChromeInPropertiesFile(String url) {
		try {
		Properties properties = new Properties();
		properties.setProperty("ChromePETableContainUrl",url);
		
		File file = new File("D:\\Edit_GenieWorkSpace\\EditGenieAutomationProject\\Configfolder\\UrlLinkGenration\\ChromeUrlGenrationForPETableContain_Footnote.properties");
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;

	}
	public static String WritePEUrlFireFoxInPropertiesFile(String url) {
		try {
		Properties properties = new Properties();
		properties.setProperty("FireFoxPETableContainUrl",url);
		
		File file = new File("D:\\Edit_GenieWorkSpace\\EditGenieAutomationProject\\Configfolder\\UrlLinkGenration\\FireForUrlGenrationForPETableContainFootnote.properties");
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;

	}

}
