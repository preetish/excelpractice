package core;

import java.io.File;
import java.io.IOException;

public class CreateNewFile {

	public static void CreateNewPropertiesFile(String path) {

		try {
			File file = new File(path);

			boolean fvar = file.createNewFile();

			if (fvar) {
				System.out.println("File has been created successfully");
			} else {
				System.out.println("File already present at the specified location");

				if (file.delete()) {
					System.out.println("File deleted successfully");

					fvar = file.createNewFile();

					if (fvar) {
						System.out.println("New File has been created successfully");
					}
				} else {
					System.out.println("Failed to delete the file");
				}
			}
		} catch (IOException e) {
			System.out.println("Exception Occurred:");
			e.printStackTrace();
		}
	}
}
