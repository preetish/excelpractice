package core;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitFor {

	public WaitFor(final WebElement element) {
	}
	
	public static WaitFor attributeContainsById(final WebDriver driver, final String id, final String attribute, final String value) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.attributeContains(By.id(id), attribute, value));
		return new WaitFor(driver.findElement(By.id(id)));
	}
	
	public static WaitFor textToBePresentInElementById(final WebDriver driver, final String id, final String text) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id(id), text));
		return new WaitFor(driver.findElement(By.id(id)));
	}

	public static WaitFor visibilityOfElementById(final WebDriver driver, final String id) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		return new WaitFor(driver.findElement(By.id(id)));
	}
	
	public static WaitFor presenceOfElementById(final WebDriver driver, final String id) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		return new WaitFor(driver.findElement(By.id(id)));
	}
	
	public static WaitFor clickableOfElementById(final WebDriver driver, final String id) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
		return new WaitFor(driver.findElement(By.id(id)));
	}
	
	public static WaitFor visiabilityOfElementByName(final WebDriver driver, final String name) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
		return new WaitFor(driver.findElement(By.name(name)));
	}
	
	public static WaitFor presenceOfElementByName(final WebDriver driver, final String name) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));
		return new WaitFor(driver.findElement(By.name(name)));
	}
	
	public static WaitFor clickableOfElementByName(final WebDriver driver, final String name) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.name(name)));
		return new WaitFor(driver.findElement(By.name(name)));
	}
	
	public static WaitFor visibilityOfElementByXpath(final WebDriver driver, final String xpath) {
		final WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return new WaitFor(driver.findElement(By.xpath(xpath)));
	}
	
	public static WaitFor presenceOfElementByXpath(final WebDriver driver, final String xpath) {
		final WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		return new WaitFor(driver.findElement(By.xpath(xpath)));
	}
	
	public static WaitFor clickableOfElementByXpath(final WebDriver driver, final String xpath) {
		final WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		return new WaitFor(driver.findElement(By.xpath(xpath)));
	}
	
	public static WaitFor visibilityOfElementByClassName(final WebDriver driver, final String className) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
		return new WaitFor(driver.findElement(By.className(className)));
	}
	
	public static WaitFor presenceOfElementByClassName(final WebDriver driver, final String className) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className(className)));
		return new WaitFor(driver.findElement(By.className(className)));
	}
	
	public static WaitFor clickableOfElementByClassName(final WebDriver driver, final String className) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.className(className)));
		return new WaitFor(driver.findElement(By.className(className)));
	}
	
	public static WaitFor visibilityOfElementByCSSSelector(final WebDriver driver, final String cssSelector) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
		return new WaitFor(driver.findElement(By.cssSelector(cssSelector)));
	}
	
	public static WaitFor presenceOfElementByCSSSelector(final WebDriver driver, final String cssSelector) {
		final WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssSelector)));
		return new WaitFor(driver.findElement(By.cssSelector(cssSelector)));
	}
	
	public static WaitFor clickableOfElementByCSSSelector(final WebDriver driver, final String cssSelector) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssSelector)));
		return new WaitFor(driver.findElement(By.cssSelector(cssSelector)));
	}
	
	public static WaitFor visibilityOfElementByLinkText(final WebDriver driver, final String linkText) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(linkText)));
		return new WaitFor(driver.findElement(By.linkText(linkText)));
	}
	
	public static WaitFor presenceOfElementByLinkText(final WebDriver driver, final String linkText) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(linkText)));
		return new WaitFor(driver.findElement(By.linkText(linkText)));
	}
	
	public static WaitFor clickableOfElementByLinkText(final WebDriver driver, final String linkText) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText(linkText)));
		return new WaitFor(driver.findElement(By.linkText(linkText)));
	}
	
	public static WebElement clickableOfElementByWebElement(final WebDriver driver, final WebElement element) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		return element;
	}
	
	public static WebElement visibilityOfElementByWebElement(final WebDriver driver, final WebElement element) {
		final WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOf(element));
		return element;
	}
	
	public static List<WebElement> visibilityOfAllElementByWebElement(final WebDriver driver, final List<WebElement> elements) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElements(elements));
		return elements;
	}
	
	public static WaitFor textToBePresentInElementByXpath(final WebDriver driver, final String xpath, final String text) {
		final WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(xpath), text));
		return new WaitFor(driver.findElement(By.xpath(xpath)));
	}
	
}