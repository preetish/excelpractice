package utilitys;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import core.MoveToElement;
import java.util.Date;
import java.util.List;




public class util {
	
	
	
	static WebDriver driver;
	
	public util(final WebElement element) {
	}
	

	
	public static void Hover(WebDriver driver, WebElement MoveToElement){
		
		    Actions action = new Actions(driver);
		    action.moveToElement(MoveToElement).click().build().perform();
		     
	}
	
	public static void HoverAndClick(WebDriver driver,
			WebElement ElementToHover,WebElement elementToClick){
		          Actions action = new Actions(driver);
      action.moveToElement(ElementToHover).click(elementToClick).build()
		            .perform();
	
	}	
	
	public static void DragAndDrop(WebDriver driver,WebElement srcElement,WebElement dstElement)
	{
	               Actions action= new Actions(driver);
	         action.dragAndDrop(srcElement, dstElement).perform();
	}
	
	
	public static util SelectDropDown(final WebDriver driver,final WebElement element,final String value)
	{	
		        Select se=new Select(element);
		        MoveToElement.byclick(driver, element);
		        se.selectByValue(value);
				core.WaitFor.clickableOfElementByWebElement(driver, element);
				MoveToElement.byclick(driver, element);
				return new util(element);
	}

	

	
	public static void explicitWait(WebDriver driver,int nbr,String Xpath){
		(new WebDriverWait(driver,nbr)).until(ExpectedConditions
				.elementToBeClickable(By.xpath(Xpath)));	
	}
	

	
	public static void SwitchWindow(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);
			for (String windowName : driver.getWindowHandles()) {
			      driver.switchTo().window(windowName);
			      System.out.println(windowName);
			      
			      
			      
			}
		}
		
		
		
public static void highLightElement(WebDriver driver,WebElement element)
{
JavascriptExecutor js=(JavascriptExecutor)driver; 
 
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
 
try 
{
Thread.sleep(500);
} 
catch (InterruptedException e) {
 
System.out.println(e.getMessage());
} 
 
js.executeScript("arguments[0].setAttribute('style','border: solid 2px white')", element); 
 
}
		



@SuppressWarnings("unused")
public static List<WebElement> isElementPresnt(WebDriver driver,String xpath,int time)
{
 

WebElement ele = null;
 
for(int i=0;i<time;i++)
{
try{
ele=driver.findElement(By.cssSelector(xpath));
break;
}
catch(Exception e)
{
try 

{
Thread.sleep(1000);
} catch (InterruptedException e1) 

{
System.out.println("Waiting for element to appear on DOM");
}

}

}
return isElementPresnt(driver, xpath, time); 
}

public static void StaleElementHandleByxpath (WebDriver driver ,String elementID){
int count = 0;
boolean clicked = false;
while (count < 4 || !clicked){
    try {
       WebElement yourSlipperyElement= driver.findElement(By.cssSelector(elementID));
       yourSlipperyElement.click(); 
       clicked = true;
     } catch (StaleElementReferenceException e){
       e.toString();
       System.out.println("Trying to recover from a stale element :" + e.getMessage());
       count = count+1;
     }     
   }
}

public String getCookieValue(String cookieName) {
	  return driver.manage()
	               .getCookieNamed(cookieName)
	               .getValue();
}

//File Read method
public static String ReadtheListOfFileAndDeleteinPass(String value) {

	File pathforvideorecord = new File(value);
File[] fileArray=  pathforvideorecord.listFiles();

for(File f:fileArray) {	
	
	           value =f.getAbsolutePath();
	System.out.println("File Location-->"+value);
	}
        return value;
       
}


public static void MoveRecFile(String value) {
	
	try {
	
	File afile =new File(value);
	 
	if(afile.renameTo(new File("E:\\EG2 Screenshot\\" + afile.getName()))){
 		System.out.println("---> File has moved successful!!!!");
 	   }else{
 		System.out.println("Failed to move the file!");
 	   }
 	    
 	}catch(Exception e){
 		e.printStackTrace();
 	}
	
}


public static void deleteRecFile(String value) {
	
	
	try {
		
		File RecFile = new File(value);
		if(RecFile.delete()){
	         System.out.println("File deleted");
	    }else {   	
	        System.out.println("File not deleted");
	         }
	 	    
	 	}catch(Exception e){
	 		e.printStackTrace();
	 	}
		
	}
	
	


public static String getCurrentDateTime()
 {
	DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	 
	 //get current date time with Date()
	 Date date = new Date();
	 
	 // Now format the date
	 String datenew= dateFormat.format(date);
	 
	 return datenew;
  }

}