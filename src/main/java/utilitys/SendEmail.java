package utilitys;

import java.io.IOException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;

public class SendEmail {

	@Test
	public void AutomaticSendMail() throws IOException {
		Zip.createZip();
		sendMail();
	}

	final static String fromAddress = DataProviderFactory.getLoginAndpathfolderConfig().EmailUser();
	final static String toAddress = DataProviderFactory.getLoginAndpathfolderConfig().toAddress();
	final static String ccAddress = DataProviderFactory.getLoginAndpathfolderConfig().ccAddress();
	final static String subject = DataProviderFactory.getLoginAndpathfolderConfig().subject();
	static String messageBody = DataProviderFactory.getLoginAndpathfolderConfig().messageBody();

	protected static void sendMail() {

		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp-mail.outlook.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(DataProviderFactory.getLoginAndpathfolderConfig().EmailUser(),
						DataProviderFactory.getLoginAndpathfolderConfig().EmailPass());
			}
		});

		try {

			// Create object of MimeMessage class
			Message message = new MimeMessage(session);

			// Set the from address
			message.setFrom(new InternetAddress(fromAddress));

			// Set To recipient address
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));

			// Set CC recipient address
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccAddress));

			// Set Subject
			message.setSubject(subject);

			BodyPart messageBodyPart = new MimeBodyPart();

			String senderName = TextFormater.properCase(fromAddress.substring(0, fromAddress.indexOf("@")));
			messageBody = messageBody.replace("</senderName>", senderName);

			String excelValue = "";
			//int excelValue1;

			// SummaryExcel va = new SummaryExcel();

			excelValue = SummaryExcel.summary();
			messageBody = messageBody.replace("</summary>", excelValue);

			messageBodyPart.setText(messageBody);

			// Create a multi-part message
			Multipart multipart = new MimeMultipart();
			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			final String fileName = DataProviderFactory.getLoginAndpathfolderConfig().attachementLocation();
			DataSource fileDataSrc = new FileDataSource(fileName);

			messageBodyPart.setDataHandler(new DataHandler(fileDataSrc));
			messageBodyPart.setFileName(fileDataSrc.getName());
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			System.out.println("\n\nTrying to send Email Please wait...");
			// Send message
			Transport.send(message);

			System.out.println("Email successfully Sent.");

		} catch (

		MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
