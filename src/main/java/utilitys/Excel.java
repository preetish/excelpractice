
package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	static XSSFWorkbook wb;
	static XSSFSheet sh;
	static XSSFCellStyle my_style;
	static String description;
	static String className;
	static String remark;
	static String category;
	static String area;
	static String status;
	static String browserName;
	static Integer num;
	final static String ExcelReportPath = ConstantPath.PathXml+"EG22\\Report\\Automation Excel Report.xlsx";

	public Excel(String description, String className, String remark, String category, String area, String status,
			String browserName) {
		Excel.description = description;
		Excel.className = className;
		Excel.remark = remark;
		Excel.category = category;
		Excel.area = area;
		Excel.status = status;
		Excel.browserName = browserName;
	}

	public void testdata(String description, String className, String remark, String category, String area,
			String status, String browserName, int sheetNbr) throws IOException {

		FileInputStream fis = new FileInputStream(new File(ExcelReportPath));

		wb = new XSSFWorkbook(fis);

		sh = wb.getSheetAt(sheetNbr);
		my_style = wb.createCellStyle();
		my_style.setFillForegroundColor(IndexedColors.RED.getIndex());
		my_style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// System.out.println(sh.getRow(0).getCell(0).getStringCellValue());

		System.out.println("sheet open");
		System.out.println("Status: " + status);
		System.out.println("Browser: " + browserName);

		// For Description

		int row = findRow(sh, description);
		System.out.println("Row number" + row);
		// row++;
		int col = 0;
		System.out.println("Row number = " + row);

		@SuppressWarnings("unused")
		String desc = null;

		if (browserName.equalsIgnoreCase("chrome")) {
			col = 5;

		} else if (browserName.equalsIgnoreCase("firefox")) {
			col = 6;

		} else if (browserName.equalsIgnoreCase("IE")) {

			col = 7;
		}

		else if (browserName.equalsIgnoreCase("edge")) {

			col = 8;
		}

		System.out.println("Col-->" + col);

		if (row == 0) {
			int rown = sh.getLastRowNum();
			System.out.println("Last Row = " + rown);
			rown++;

			sh.createRow(rown);
			sh.getRow(rown).createCell(0).setCellValue(description);
			sh.getRow(rown).createCell(1).setCellValue(className);

			sh.getRow(rown).createCell(3).setCellValue(category);
			sh.getRow(rown).createCell(4).setCellValue(area);
			sh.getRow(rown).createCell(col).setCellValue(status);

			// String temp = null;
			if (status.equals("Fail") || status.equals("fail")) {

				// Cell c = sh.getRow(rown).getCell(2);

				// sh.getRow(rown).getCell(col).setCellStyle(my_style);

				// temp = sh.getRow(row+1).getCell(2).getStringCellValue();

				System.out.println("Remark = " + remark);
				// if (c == null || c.getCellType() == Cell.CELL_TYPE_BLANK) {
				sh.getRow(rown).createCell(2).setCellValue(browserName + ":" + remark + "...");
				// }
				// else{

				// temp = sh.getRow(row).getCell(2).getStringCellValue();
				// sh.getRow(row).createCell(2).setCellValue(temp + browser + ":" + remark +
				// "...");
				sh.getRow(rown).getCell(col).setCellStyle(my_style);

				// }
			}
		} else {

			System.out.println("Row value = " + row);

			sh.getRow(row).createCell(col).setCellValue(status);

			String temp = null;

			if (status.equals("Fail") || status.equals("fail")) {

				Cell c = sh.getRow(row).getCell(2);
				sh.getRow(row).getCell(col).setCellStyle(my_style);
				// temp = sh.getRow(row+1).getCell(2).getStringCellValue();

				if (c == null || c.getCellType() == Cell.CELL_TYPE_BLANK) {
					sh.getRow(row).createCell(2).setCellValue(browserName + ":" + remark + "...");
				} else {

					temp = sh.getRow(row).getCell(2).getStringCellValue();
					sh.getRow(row).createCell(2).setCellValue(temp + browserName + ":" + remark + "...");
					sh.getRow(row).getCell(col).setCellStyle(my_style);

				}
			}
		}

		// For saving the excel sheet
		FileOutputStream fos = new FileOutputStream(new File(ExcelReportPath));
		wb.write(fos);

		fos.close();
	}

	private static int findRow(XSSFSheet sh, String cellContent) {
		for (Row row : sh) {
			for (Cell cell : row) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {

						System.out.println(cell.getRichStringCellValue().getString().trim());
						System.out.println(cellContent);
						return row.getRowNum();
					}
				}
			}
		}

		return 0;
	}
}