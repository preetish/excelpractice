package utilitys;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;

public class StringExtractor {

	public static StringBuilder stringBuilder = new StringBuilder();

	public enum mSearchDirection {
		START, END
	}

	final static String SEPERATOR = " ";

	public static String nthWordFrom(WebElement element, mSearchDirection searchDir, int skipNoOfWords,
			int fetchNoOfWords) {
		String str = element.getText();
		List<String> wordList = str != null ? Arrays.asList(str.split(SEPERATOR)) : null;
		StringBuilder strBuilder = new StringBuilder();
		if (wordList != null) {

			int startIndex = searchDir == mSearchDirection.START ? skipNoOfWords : (wordList.size() - skipNoOfWords);
			int endIndex = searchDir == mSearchDirection.START ? (skipNoOfWords + fetchNoOfWords)
					: (wordList.size() - skipNoOfWords) + fetchNoOfWords;
			endIndex = endIndex > wordList.size() ? wordList.size() : endIndex;

			for (int i = startIndex; i < endIndex; i++) {
				strBuilder.append(wordList.get(i) + ((i < endIndex - 1) ? " " : ""));
			}
		}
		return strBuilder.toString();
	}

	public static int countString(WebElement element) {
		String str = element.getText();

		// str = str.substring(str.length() - 7).contains(")") ? str.substring(0,
		// str.length() - 7).trim() : str;

		/*
		 * str = str.charAt(0) == '[' ? str.substring(str.indexOf("]") + 1) : str; if
		 * (str.contains("[")) { str = str.substring(0, str.indexOf("[")); }
		 */
		
		System.out.println("String data-->" + str);	
		System.out.println("String Count-->" + str.length());
		
		int countString = str.length();
		return countString > 120 ? 120 : countString;
	}

	
	public static String stitchDelStr(String data) {
		data = data.replaceAll("<del", "\n<del");
		final String strPattern = "(>.+</del>)+";
		Pattern pattern = Pattern.compile(strPattern);
		Matcher m = pattern.matcher(data);
		stringBuilder.setLength(0);

		while (m.find()) {
			String temp = m.group();
			stringBuilder.append(temp.substring(temp.indexOf(">") + 1, temp.indexOf("</del>")));
		}
		String data1 = stringBuilder.toString().isEmpty() ? data : stringBuilder.toString();
		return data1;
	}

	public static String stitchInsStr(String data) {
		data = data.replaceAll("<ins", "\n<ins");
		final String strPattern = "(>.+</ins>)+";
		Pattern pattern = Pattern.compile(strPattern);
		Matcher m = pattern.matcher(data);
		stringBuilder.setLength(0);

		while (m.find()) {
			String temp = m.group();
			stringBuilder.append(temp.substring(temp.indexOf(">") + 1, temp.indexOf("</ins>")));
		}
		String data1 = stringBuilder.toString().isEmpty() ? data : stringBuilder.toString();
		return data1;
	}
}
