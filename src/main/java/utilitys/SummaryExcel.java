package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SummaryExcel {

	final private static String excelFilePath = ConstantPath.PathXml + "EG22\\Report\\Automation Excel Report.xlsx";
	static int mHeaderRowNo = DataProviderFactory.getLoginAndpathfolderConfig().getHeaderRowNo();
	static String[] mColumnNos = DataProviderFactory.getLoginAndpathfolderConfig().getColumnToCheck();

	static LinkedHashMap<String, Integer> getLinkedHashMap(Sheet sheet) {
		LinkedHashMap<String, Integer> temp = new LinkedHashMap<>();
		for (String columnNo : mColumnNos) {
			String key = sheet.getRow(mHeaderRowNo).getCell(Integer.valueOf(columnNo)).getStringCellValue();
			temp.put(key + " Pass", 0);
			temp.put(key + " Fail", 0);
		}
		return temp;
	}

	@SuppressWarnings("unused")
	public static String summary() {
		StringBuilder tempBuilder = new StringBuilder();
		try {
			System.out.println("Processing File at location : " + excelFilePath);
			FileInputStream excelFile = new FileInputStream(new File(excelFilePath));
			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(excelFile);
			String[] sheetNos = DataProviderFactory.getLoginAndpathfolderConfig().getSummaryForSheetsNo();

			for (String sheetNo : sheetNos) {
				int totalTestCase = 0;
				Sheet sheet = workbook.getSheetAt(Integer.valueOf(sheetNo));
				LinkedHashMap<String, Integer> tempLMap = getLinkedHashMap(sheet);

				for (String columnNo : mColumnNos) {
					String mainKey = sheet.getRow(mHeaderRowNo).getCell(Integer.valueOf(columnNo)).getStringCellValue();

					for (Row row : sheet) {

						if (row.getRowNum() > mHeaderRowNo && row.getCell(1) != null
								&& !row.getCell(1).getStringCellValue().isEmpty()) {

							totalTestCase++;

							String status = row.getCell(Integer.valueOf(columnNo)) == null ? "No Status"
									: row.getCell(Integer.valueOf(columnNo)).getStringCellValue();

							if (tempLMap.get(mainKey + " " + status) == null) {
								tempLMap.put(mainKey + " skip", (tempLMap.get(mainKey + " skip") == null ? 1
										: tempLMap.get(mainKey + " skip") + 1));
							} else {
								tempLMap.put(mainKey + " " + status, tempLMap.get(mainKey + " " + status) + 1);
							}
						}
					}
				}
				tempBuilder.append(
						"\n\n" + sheet.getSheetName() + " Total Test cases : " + calculateTotalTest(tempLMap) + "\n");
				tempLMap.forEach((k, v) -> {
					tempBuilder.append(k + " : " + v + "\n");
				});
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return tempBuilder.toString();
	}

	private static int calculateTotalTest(LinkedHashMap<String, Integer> tempLMap) {
		int totalCount = 0;
		for (Map.Entry<String, Integer> entry : tempLMap.entrySet()) {
			if (entry.getKey().split(" ")[0].equalsIgnoreCase("CHROME")) {
				totalCount += tempLMap.get(entry.getKey());
			}
		}
		return totalCount;
	}
}
