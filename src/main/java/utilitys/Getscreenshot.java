package utilitys;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Getscreenshot {

	public static String captureScreenShot(String browserName, String className, WebDriver driver) {

		String path = "E:\\EG2 Screenshot/" + browserName + "" + className + ".png";

		try {

			Thread.sleep(3000);

			File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshotFile, new File(path));
			System.out.println("ScreenshotTaken");

		} catch (Exception e) {
			System.out.println("Exception will takeing screenshot " + e);
		}
		return path;
	}
	
	
	public static String captureScreenShotJunk(String EgStage, String browserName, String className, WebDriver driver) {

		String path = ConstantPath.PathXml+"EG22\\Report/"+EgStage+"/" + browserName + "" + className + ".png";

		try {

			Thread.sleep(3000);

			File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshotFile, new File(path));
			System.out.println("ScreenshotTaken");

		} catch (Exception e) {
			System.out.println("Exception will takeing screenshot " + e);
		}
		return path;
	}
	
}