package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigDataProvider 
{
	Properties pro;
	
	
	public ConfigDataProvider()
	{
		
		File src=new File("./Configfolder/config.properties");
		
		try 
		{
			FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		{
			System.out.println("Exception is "+e.getMessage());
		}
		
	}
	
	public String getIEPath()
	{
		String url=pro.getProperty("IEPath");
		return url;
	}
	
	
	public String getChromePath()
	{
		String url=pro.getProperty("chromePath");
		return url;
	}
	
	public String getFireFoxGekoPath()
	{
		String url=pro.getProperty("FireFoxGekoPath");
		return url;
	}
	
	public String getOperaPath()
	{
		String url=pro.getProperty("OperaPath");
		return url;
	}
	
	
	public String getApplicationUrl()
	{
		String url=pro.getProperty("url");
		return url;
	}
	
	public String getApplicationUrl1()
	{
		String url1=pro.getProperty("url1");
		return url1;
	}
	
	
	public String getApplicationFireFoxTableContainUrl1()
	{
		String url1=pro.getProperty("FireFoxTableContainUrl");
		return url1;
	}
	
	public String getApplicationFireFoxPETableContainUrl2()
	{
		String url1=pro.getProperty("FireFoxEditorEndTableContainUrl");
		return url1;
	}
	

	public String getApplicationFireFoxFigureContainUrl() {
		String url1=pro.getProperty("FireFoxPEFigureContainUrl");
		return url1;
	}
	
	public String getApplicationAEFireFoxFigureContainUrl1() {
		String url1=pro.getProperty("FireFoxFigureContainUrl");
		return url1;
	}
	
	
	public String getLinkGenration()
	{
		String linkGenrate=pro.getProperty("linkGenration");
		return linkGenrate;
	}
	
	
	
	
	
	
	public String getFtpURL()
	{
		String linkGenrate=pro.getProperty("FTP_url");
		return linkGenrate;
	}
	
	public String getFtp_ip()
	{
		String Ftpip=pro.getProperty("FTP_i.p");
		return Ftpip;
	}
	
	public String getFtpusername()
	{
		String username=pro.getProperty("Ftpusername");
		return username;
	}
	
	public String getFtpPassword()
	{
		  String password=pro.getProperty("Ftppasword");
		return password;
	}
	
	
	
	
	public String getFTP_author_Folder()
	{
		  String folder=pro.getProperty("FTP_author_Folder");
		return folder;
	}
	
	public String getFTPCreateArticleFolderdownloadFiles()
	{
		  String folder=pro.getProperty("Ftp_CreateArticleFolder_download");
		return folder;
	}
	
	public String getFTP_Ftp_insideFolderFiles()
	{
		  String files=pro.getProperty("Ftp_insideFolderFiles");
		return files;
	}
	
	public String getFTP_Ftp_PublishedFile()
	{
		  String files=pro.getProperty("Ftp_PublishedFile");
		return files;
	}

	
	
	public String getFTP_author_remoteUrl()
	{
		  String password=pro.getProperty("FTP_author_remoteUrl");
		return password;
	}
	
	public String getFTP_downloadAuthorFile()
	{
		  String author=pro.getProperty("FTP_downloadAuthorFile");
		return author;
	}
	
	public String getFTP_OrigialTandf_remoteUrl()
	
	{
		  String tandf=pro.getProperty("FTP_OriginalTandf_remoteUrl");
		return tandf;
	}
	
	
	public String getFTP_downloadoriginalAuthorTandf()
	{
		  String password=pro.getProperty("FTP_downloadoriginalAuthorTandf");
		return password;
	}
	
	
	
	
	
	public String getExelSheetPath()
	{
		  String password=pro.getProperty("FTP_downloadFile");
		return password;
	}
	
	
	public String getps() {
		 String ps=pro.getProperty("ps");
			return ps;
	}
	
	public String getSpecialCharValidation() {
		 String ps=pro.getProperty("SpecialCharValidation");
			return ps;
	}
	
	
}