package com.PracticeInterviewExcelTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GetdataFromExcel {

	final static String excelPath = "C:\\Users\\Preetish Kumar\\Downloads\\EG22\\src\\test\\java\\com\\PracticeInterviewExcelTest\\datatest.xlsx";

	public static void main(String[] args) {

		GetdataFromExcel rc = new GetdataFromExcel(); // object of the class

		GetdataFromExcel.iteratorExcel();

		String vOutput = rc.readCellData(2, 0);
		System.out.println(vOutput);

	}

	public String readCellData(int Row, int Column) {
		String value = null;
		Workbook wb = null;

		try {
			FileInputStream fis = new FileInputStream(excelPath);

			wb = new XSSFWorkbook(fis);
		} catch (IOException e) {

			e.printStackTrace();
		}

		Sheet sheet = wb.getSheetAt(0);

		Row row = sheet.getRow(Row);
		Cell cell = row.getCell(Column);

		value = cell.getStringCellValue();
		return value;
	}

	public static void iteratorExcel() {

		try {

			FileInputStream file = new FileInputStream(new File(excelPath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						System.out.print(cell.getNumericCellValue() + "|");
						break;
					case Cell.CELL_TYPE_STRING:
						System.out.print(cell.getStringCellValue() + "|");
						break;
					}
				}
				System.out.println("");
			}
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
